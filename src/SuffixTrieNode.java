import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SuffixTrieNode {
    final static int MAX_CHAR = 256;
    SuffixTrieNode[] children = new SuffixTrieNode[MAX_CHAR]; //Array of Nodes
    List<Integer> indexes;

    //Constructor
    public SuffixTrieNode() {
        //Creating an empty linkedList for indexes of
        // suffixes starting from this node
        indexes = new LinkedList<Integer>();

        //Initialize all childpointers as NULL
        for (int i = 0; i < MAX_CHAR; i++) {
            children[i] = null;
        }

    }

    // A recursive function to insert a suffix of
    // the text in subtree rooted with this node
    void insertSuffix(String s, int index) {
        indexes.add(index); //Store index in linkedlist
        //If String has more Characters
        if (s.length() > 0) {
            //Find the first Character
            char cInex = s.charAt(0);
            //If there is no edge for this character
            //Add a new edge (a new Node)
            if (children[cInex] == null) {
                children[cInex] = new SuffixTrieNode();
                //Recur for next suffix
                children[cInex].insertSuffix(s.substring(1), index + 1);
            }
        }

        // A function to search a pattern in subtree rooted
        // with this node.The function returns pointer to a
        // linked list containing all indexes where pattern
        // is present. The returned indexes are indexes of
        // last characters of matched text.
    }

    // A function to search a pattern in subtree rooted
    // with this node.The function returns pointer to a
    // linked list containing all indexes where pattern
    // is present. The returned indexes are indexes of
    // last characters of matched text.
    List<Integer> searche(String s) {
        //If all characters has been processed
        if (s.length() == 0) {
            return indexes;
        }
        //If there is an edge from the current node of
        //suffix tree, follow the edge
        if (children[s.charAt(0)] != null) {
            return (children[s.charAt(0)].searche(s.substring(1)));
            //If there is no edge, pattern dosnt exist in text
        } else return null;
    }
}

class Suffix_tree {
    SuffixTrieNode root = new SuffixTrieNode();

    // Constructor (Builds a trie of suffies of the
    // given text)
    Suffix_tree(String txt) {

        // Consider all suffixes of given string and
        // insert them into the Suffix Trie using
        // recursive function insertSuffix() in
        // SuffixTrieNode class 
        for (int i = 0; i < txt.length(); i++) {
            root.insertSuffix(txt.substring(i), i);
        }
        /* Prints all occurrences of pat in the Suffix Trie S(built for text) */


    }

    void search_tree(String pat) {
        // Let us call recursive search function for
        // root of Trie.
        // We get a list of all indexes (where pat is
        // present in text) in variable 'result'
        List<Integer> result = root.searche(pat);
        // Check if the list of indexes is empty or not
        if (result == null) {
            System.out.println("Pattern not found");
        } else {
            int patLen = pat.length();
            for (Integer i : result) {
                System.out.println("Pattern found at " + (i - patLen));

            }
        }

    }

    public static void main(String[] args) {

        ArrayList<String> myarr=new ArrayList<>();
        myarr.add("Geek");
        myarr.add("Java");
        Suffix_tree S=new Suffix_tree(myarr.toString());
        for (int i = 0; i <myarr.size() ; i++) {
            S.search_tree(myarr.get(i));

        }


    }
}
