import java.util.*;

import static java.util.Arrays.*;

public class Trie {
    private Node rootNode;
    Trie(){
        rootNode=null;
    }
    //Tar in stringen från arrayen och loopar igenom det char för char
    void insert(String key, int index){
        if (rootNode==null){
            rootNode=new Node();
        }
        Node currentNode=rootNode;
        for (int i = 0; i <key.length() ; i++) {
            char keyChar=key.charAt(i);
            if (currentNode.getChild(keyChar)==null){
                currentNode.addChild(keyChar); //Adderar charen om den finns med värdet null, dvs.
            }
            //Go to next node
            currentNode=currentNode.getChild(keyChar);
        }
        /*Mark leaf (End of string)
        and store index of 'str' in index[]*/
        currentNode.addIndex(index);
    }
    void traversePreorder(String [] array){
        traversePreorder(rootNode,array);
    }
    /*function for preOrder traversal of trie
    */
    private void traversePreorder(Node node,String [] array){
        if (node==null){
            return;
        }
        if (node.getIndices().size()>0){
            for (int index:node.getIndices()){
                System.out.print(array[index]+" ");
            }
        }
        for (char index= 'A';index<='z';index++) {
            traversePreorder(node.getChild(index),array);
        }
    }
    private static class Node{
        private Node[] children;
        private List<Integer> indices;
        Node(){
            children=new Node[127];
            indices=new ArrayList<>(0);
        }
        Node getChild(char index)
        {

            if (index<'A'|| index >'z'){
                return null;
            }

            return children[index - 'A'];
        }

        void addChild(char index){
            if (index<'A'|| index>'z'){
                return;
            }
            Node node=new Node();
            children[index - 'A']=node;
        }
        List<Integer> getIndices(){
            return indices;
        }
        void addIndex(int index){
            indices.add(index);
        }
    }
}
class SortStrings {
    //Driver program
    public static void main(String[] args) {
List<String> nameArr=new ArrayList<>();

        String[] array={"Anara","Casper","Martin","Khazar","Isabelle","Malin","Ludwig","Jesse","Daniel","David","Emil","Gustav"
        ,"Henrik","Hilda","Jasmin","Joakim","Simon","Daniel","Lydia","Julia","Max","Mohammad","Lydia","Patrik","Peter","Timmy","Ann","Adam"};
        nameArr.addAll(Arrays.asList(array));
        Collections.shuffle(nameArr);
       // System.out.println("Randomordning\n"+nameArr+"\n");
        System.out.println("Sorterad lista");
        printInsortedOrder(array);
    }
    //function to sort an array of string using trie
    public static void printInsortedOrder(String[] array) {
        Trie trie=new Trie();
        for (int i = 0; i <array.length ; i++) {
            trie.insert(array[i], i);
        }
        trie.traversePreorder(array);

    }
}
